from flask import redirect
from flask.globals import request

from forwarder.config import FALLBACK_URL 
from forwarder import app, database


@app.route("/")
def hello_world():
    """
    Redirects to the fallback URL.
    :returns: A redirection to the fallback URL.
    """
    return redirect(FALLBACK_URL)


@app.route("/url/new", methods=["POST"])
def add_url():
    """
    Adds a new URL.

    This method requires an HTTP POST request containing a 'url' and a 'token'
    attribute.

    :returns: 201 if the URL has been shortened successfully, otherwise 403.
    """
    url = request.form.get("url")
    token = request.form.get("token")
    status = database.add_url(url, token)
    return status


@app.route("/<token>")
def forward(token):
    """
    Redirects to the URL of a specific token.
    :param token: The token of the URL in interest.
    :returns: A redirection to the specific website.
    """
    result = database.get_url(token)
    if not result:
        return redirect(FALLBACK_URL)
    return redirect(result)