import os
import sqlite3

from forwarder.config import DATABASE_FILE


def connect():
    """
    Establishes a database connection.
    """
    if not os.path.exists(DATABASE_FILE):
        initialize()
    connection = sqlite3.connect(DATABASE_FILE)
    return connection


def initialize():
    """
    Initializes the database.
    """
    connection = sqlite3.connect(DATABASE_FILE)
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE redirections(token TEXT PRIMARY KEY, url TEXT)")
    connection.commit()
    connection.close()


def get_url(token):
    """
    Receives a URL for a given token.

    :param token: The token of the URL in interest.
    :returns: The URL, if one exists, otherwise None.
    """
    connection = connect()
    cursor = connection.cursor()
    cursor.execute("SELECT url FROM redirections WHERE token = ?", [token])
    result = cursor.fetchone()
    connection.close()
    return result[0] if result else None


def add_url(url, token):
    """
    Adds a new URL to the database.

    :param url: The URL.
    :param token: The token that should map to the URL.
    :returns:
        - 403 if the token already exists
        - 201 if the URL was successfully shortened
    """
    # Check if token is already taken
    check_url = get_url(token)
    if check_url:
        return "403", 403
    
    connection = connect()
    cursor = connection.cursor()
    cursor.execute("INSERT INTO redirections VALUES (?, ?)", [token, url])
    connection.commit()
    connection.close()
    return "201", 201
